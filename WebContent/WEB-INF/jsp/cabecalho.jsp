<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Bem-vindo - Agenda de Contatos</title>

<link rel="stylesheet" href="css/jquery-ui.min.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/estilo.css">
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>

</head>
<body>

<div id="centro">
<div id="principal">
<div class="jumbotron jumbotron-fluid">
  <div class="container">
    <h1 class="display-3">Bem-vindo - Agenda de Contatos</h1>
  
  </div>
</div>
</div>
<img src="imagens/logo.jpg" width="200px" height="50px"/>

</div>
<p> Data: <%= new SimpleDateFormat("dd/MM/yyy"). 
	format(new Date()) %>
	</p>

<p>
	Hora: <%=new SimpleDateFormat("hh:mm:ss").
	format(new Date())%>
</p>

<hr>
</body>
</html>