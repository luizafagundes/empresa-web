<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cadastro de Funcionarios</title>

<link rel="stylesheet" href="css/jquery-ui.min.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/estilo.css">
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>

</head>
<body style="background-color: #C5A7C9"">

	<div id="centro">
		<header>
			<div id="principal" style="margin-bottom: 0px"
				class="jumbotron centro jumbotron-fluid font-color-jumbotron">
				<div class="container">
					<h1 class="display-3">Bem-Vindo!</h1>
					<p class="lead">Cadastre-se no nosso sistema de funcionários.</p>
				</div>
			</div>
	</div>
	</header>




	<div class="container">
		<form action="mvc?logica=SalvaFuncionarioLogica" method="post">
			<!--Campo de primeiro nome-->
			<div class="form-group">
				<label for="nome" class="font-color">Nome</label> <input type="text"
					name="nome" class="form-control" id="nome"
					placeholder="Digite seu nome">
			</div>

			<!--Campo de Email-->
			<div class="form-group">
				<label for="email" class="font-color">Email</label> <input
					type="text" name="email" class="form-control" id="email"
					placeholder="Digite seu email">
			</div>

			<!--Campo de CPF-->
			<div class="form-group">
				<label for="cpf" class="font-color">CPF</label> <input type="text"
					name="cpf" class="form-control" id="cpf"
					placeholder="Digite seu CPF">
			</div>

			<!--Campo de Senha-->
			<div class="form-group">
				<label for="senha" class="font-color">Senha</label> <input
					type="password" name="senha" class="form-control" id="senha"
					placeholder="Digite sua senha">
			</div>
			<div class="centro">
				<button type="submit" class="btn btn-secondary">salvar</button>
			</div>
	</div>
	</form>
	</div>

	<br> Copyright&copy; 2017 - Todos os direitos reservados


	<!-- form action="mvc?logica=SalvaFuncionarioLogica" method="post">
			Nome: <br /> <input type="text" name="nome" /> <br /> <br />
			E-mail: <br /> <input type="text" name="email" /> <br /> <br />
			CPF: <br /> <input type="text" name="cpf" /> <br /> <br /> Senha:
			<br /> <input type="password" name="senha" /><br /> <input
				type="submit" name="Salvar" /> <br /> <br />
		</form-->
</body>
</html>