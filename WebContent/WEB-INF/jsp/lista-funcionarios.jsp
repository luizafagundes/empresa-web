<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%-- cabeçalho da taglib core --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cadastro de funcionarios</title>
<link rel="stylesheet" href="css/jquery-ui.min.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/estilo.css">
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
</head>
<body style="background-color: #C5A7C9">
	<div id="centro">
		<header>
			<div id="principal" style="margin-bottom: 0px"
				class="jumbotron centro jumbotron-fluid font-color-jumbotron">
				<div class="container">
					<h1 class="display-3">EMPRESA WEB</h1>
					<p class="lead">Lista de funcionarios</p>
				</div>
			</div>
	</div>

	</header>

	<br>
	<table border="1" style="border-collapse: colappse; margin-left: 600px">

		<tr>
			<th>Nº</th>
			<th>Nome</th>
			<th>E-mail</th>
			<th>CPF</th>
			<th colsapan="2">Opções</th>

			<c:forEach var="funcionario" items="${funcionario }" varStatus="id">
				<tr bgcolor="#${id.count % 2 == 0 ? 'B6789E' : 'DEB7CF' }">
					<td>${id.count }</td>
					<td>${funcionario.nome }</td>
					<td><c:if test="${not empty funcionario.email }">
							<a href="mailto:${funcionario.email}"> ${funcionario.email }
							</a>
						</c:if> <c:if test="${empty funcionario.email}">
					E-mail não informado
				</c:if></td>
					<td>${funcionario.cpf}</td>

					<td><a
						href="mvc?logica=RemoveFuncionario&id=${funcionario.id }">
							Excluir</a> <a
						href="mvc?logica=ExibeFuncionarioLogica&id=${funcionario.id }">
							Atualizar</a>
			</c:forEach>
	</table>
	<br>
	<div id="centro">
	 <a href= "mvc?logica=AdicionaFuncionarioLogica" type="submit" class="btn btn-secondary">Adicionar funcionarios </a>
	</div>
	<c:import url="rodape.jsp"></c:import>
</body>
</html>
