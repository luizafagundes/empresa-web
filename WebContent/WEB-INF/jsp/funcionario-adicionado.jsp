<%@ page language="java" contentType="text/html; charset= UTF-8"
	pageEncoding="UTF-8"%>
<link rel="stylesheet" href="css/jquery-ui.min.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/estilo.css">
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>

</head>
<body style="background-color: #C5A7C9">

	<div id="centro">
		<header>
			<div id="principal" style="margin-bottom: 0px"
				class="jumbotron centro jumbotron-fluid font-color-jumbotron">
				<div class="container">
					<h1 class="display-3">EMPRESA WEB</h1>

				</div>
			</div>

		</header>
		<br> Funcionario ${param.nome} adicionado com sucesso!
		<br>
		<br>
		<a href= "mvc?logica=ListaFuncionariosLogica" type="submit" class="btn btn-secondary">Listar funcionarios </a>
	</div>
</body>
</html>