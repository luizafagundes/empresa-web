<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>	SEJA BEM VINDO...</title>
</head>
<body>
<%-- comentário jsp --%>
<%--declara uma variavel do tipo string --%>
<%String mensagem = "Bem vindo ao sistema de empresa"; %>

<%-- escreve a mensagem --%>
<%=mensagem %>
<br/><br/>
<%-- imprime a data e horas atuais --%>
<%=new SimpleDateFormat ("dd/MM/yyyy").format(new Date()) %> 
<br/> <br/>
<%=new SimpleDateFormat ("hh:mm:ss").format(new Date()) %>
</body>
</html>