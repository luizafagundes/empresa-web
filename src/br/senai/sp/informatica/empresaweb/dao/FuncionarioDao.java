/**
 * @author Luiza Fagundes
 
 */

package br.senai.sp.informatica.empresaweb.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.senai.sp.informatica.empresaweb.model.Funcionario;

public class FuncionarioDao {
	private Connection connection;

	public FuncionarioDao() {
		// cria uma ConnectionFactory
		connection = new ConnectionFactory().getConnection();
	}

	public void salva(Funcionario funcionario) {
		String sql = null;

		// Se o ID for igual a nulo, �a o update
		if (funcionario.getId() != null) {

			// O cotato possui um id portanto fa�a um update
			sql = "UPDATE funcionario SET nome = ?, email = ?, cpf = ?, senha = ? WHERE id= ?";

		} else {
			// cria o que colocar na tabela
			sql = "INSERT INTO funcionario " + "(nome, email, cpf, senha) VALUES " + "(?, ?, ?, ?)";
		}
			try {
				PreparedStatement stmt = connection.prepareStatement(sql);
				stmt.setString(1, funcionario.getNome());
				stmt.setString(2, funcionario.getEmail());
				stmt.setString(3, funcionario.getCpf());
				stmt.setString(4, funcionario.getSenha());
				if (funcionario.getId() != null) {
					stmt.setLong(5, funcionario.getId());
				}
				stmt.execute();
				stmt.close();
			} catch (SQLException e) {
				throw new RuntimeException(e);
			} finally {
				try {
					connection.close();
				} catch (SQLException e) {
					throw new RuntimeException(e);
				}
			}
		}

	public List<Funcionario> getLista() {
		try {
			List<Funcionario> funcionarios = new ArrayList<>();
			PreparedStatement stmt = connection.prepareStatement("SELECT * FROM funcionario");
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				Funcionario funcionario = new Funcionario();
				funcionario.setId(rs.getLong("id"));
				funcionario.setNome(rs.getString("nome"));
				funcionario.setEmail(rs.getString("email"));
				funcionario.setCpf(rs.getString("cpf"));
				funcionario.setSenha(rs.getString("senha"));
				funcionarios.add(funcionario);
			}
			rs.close();
			stmt.close();
			return funcionarios;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
	}

	public void excluir(Funcionario funcionario) {
		try {
			PreparedStatement stmt = connection.prepareStatement("Delete FROM funcionario WHERE id = ?");
			stmt.setLong(1, funcionario.getId());
			stmt.execute();
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}

	}

}
