/**
 * @author Luiza Fagundes
 */

package br.senai.sp.informatica.empresaweb.servlet;


import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.senai.sp.informatica.empresaweb.dao.FuncionarioDao;
import br.senai.sp.informatica.empresaweb.model.Funcionario;
import br.senai.sp.informatica.empresaweb.dao.FuncionarioDao;
import br.senai.sp.informatica.empresaweb.model.Funcionario;

@WebServlet("/adicionaFuncionario")
public class AdicionaFuncionarioServlet extends HttpServlet {

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		// PrintWriter
		PrintWriter leitor = res.getWriter();

		// pega os par�metros da requisi��o
		String nome = req.getParameter("nome");
		String email = req.getParameter("email");
		String cpf = req.getParameter("cpf");
		String senha = req.getParameter("senha");

		// cria um funcionario
		Funcionario funcionario = new Funcionario();
		funcionario.setNome(nome);
		funcionario.setEmail(email);
		funcionario.setCpf(cpf);
		funcionario.setSenha(senha);

		// inst�ncia o funcionario dao
		// abre uma conex�o com o banco de dados
		FuncionarioDao dao = new FuncionarioDao();

		// salva o funcionario no banco de dados
		dao.salva(funcionario);

		// feedback para o usu�rio

		// Cria um request dispatcher
		RequestDispatcher dispatcher = req.getRequestDispatcher("/funcionario-adicionado.jsp");

		// encaminha usuario para essa pagina
		dispatcher.forward(req, res);

		/*
		 * out.print("<html>"); out.print("<body>"); out.print("funcionario " +
		 * funcionario.getNome() + " adicionado com sucesso !"); out.print("</body>");
		 * out.print("</html>");
		 */
	}
}
