/**
 * @author Luiza Fagundes
 */

package br.senai.sp.informatica.empresa.mvc.logica;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface Logica {
	
	public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception;
	
	/**
	 * Este m�todo retorna uma pagina jsp
	 * para que o dispatcher fa�a o encaminhamento
	 * @param req requisi��o do usu�rio
	 * @param res resposta do servidor
	 * @return pagina jsp para ser exibida
	 * @throws Exception erros que podem ocorrer
	 */
}
