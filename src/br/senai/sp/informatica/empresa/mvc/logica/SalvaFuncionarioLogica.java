/**
 * @author Luiza Fagundes
 */

package br.senai.sp.informatica.empresa.mvc.logica;

import br.senai.sp.informatica.empresaweb.model.Funcionario;
import br.senai.sp.informatica.empresa.mvc.logica.Logica;
import br.senai.sp.informatica.empresaweb.dao.FuncionarioDao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SalvaFuncionarioLogica implements Logica {

	public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception {

		// Obt�m os dados do form atrav�s do request
		String nome = req.getParameter("nome");
		String email = req.getParameter("email");
		String cpf = req.getParameter("cpf");
		String senha = req.getParameter("senha");

		// Cria uma String de funcionario
		Funcionario funcionario = new Funcionario();

		// Atribui os valores do form ao funcionario
		funcionario.setNome(nome);
		funcionario.setEmail(email);
		funcionario.setCpf(cpf);
		funcionario.setSenha(senha);

		// Cria uma inst�ncia de Funcionario Dao
		FuncionarioDao dao = new FuncionarioDao();

		// Salva o funcionario no banco de dados
		dao.salva(funcionario);

		// Exibe a p�gina feedback ao usu�rio
		// obs: voc� pode exibir qualuqer outra p�gina, inclusive a lista de
		// funcionarios

		return "WEB-INF/jsp/funcionario-adicionado.jsp";

	}

}
