/**
 * @author Luiza Fagundes
 */

package br.senai.sp.informatica.empresa.mvc.logica;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.senai.sp.informatica.empresaweb.dao.FuncionarioDao;
import br.senai.sp.informatica.empresaweb.model.Funcionario;

public class ExibeFuncionarioLogica implements Logica {

	@Override
	public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception {

		// Cria uma vari�vel do tipo funcionario
		Funcionario funcionario = null;

		// Recupera o ID por parametro
		long id = Long.parseLong(req.getParameter("id"));

		// TODO mover para Dao
		List<Funcionario> funcionarios = new FuncionarioDao().getLista();

		// Percorre a lista de funcionarios testando achando uma coincidencia de Id de
		// parametro
		for (Funcionario c : funcionarios) {

			if (c.getId() == id) {
				funcionario = c;
				break;
			}

		}

		// Associa os atributos do funcionario a atributos da sess�o (request)
		req.setAttribute("id", funcionario.getId());
		req.setAttribute("nome", funcionario.getNome());
		req.setAttribute("email", funcionario.getEmail());
		req.setAttribute("cpf", funcionario.getCpf());
		req.setAttribute("senha", funcionario.getSenha());

		return "WEB-INF/jsp/exibe-funcionario.jsp";

	}

}