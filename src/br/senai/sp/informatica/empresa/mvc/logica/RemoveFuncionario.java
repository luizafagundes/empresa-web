/**
 * @author Luiza Fagundes
 */

package br.senai.sp.informatica.empresa.mvc.logica;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.senai.sp.informatica.empresaweb.dao.FuncionarioDao;
import br.senai.sp.informatica.empresaweb.model.Funcionario;

public class RemoveFuncionario implements Logica {

	@Override
	public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception {
		
		long id = Long.parseLong(req.getParameter("id"));
		Funcionario funcionario = new Funcionario ();
		funcionario.setId(id);
		
		FuncionarioDao dao = new FuncionarioDao();
		dao.excluir(funcionario);
		
		System.out.println("Excluindo funcionario...");
		
		return "mvc?logica=ListaFuncionariosLogica";
		
		
		
	}

}
