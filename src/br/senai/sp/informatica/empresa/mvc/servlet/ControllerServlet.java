/**
 * @author Luiza Fagundes
 */

package br.senai.sp.informatica.empresa.mvc.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.senai.sp.informatica.empresa.mvc.logica.Logica;

@WebServlet("/mvc")

public class ControllerServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		// Obt�m o par�metro logica da requisi��o
		// http://localhost:8080/mvc?logica=spto
		String parametro = req.getParameter("logica");

		// como � o nome da classe l�gica
		// utilizando o par�metro
		// br.senai.informatica.agenda.mvc.logica.xpto
		String className = "br.senai.sp.informatica.empresa.mvc.logica." + parametro;

		try {
			// carrega a classe para a mem�ria
			Class classe = Class.forName(className);

			// cria uma nova inst�ncia da classe
			// de l�gica que est� na mem�ria
			Logica logica = (Logica) classe.newInstance();

			// executa o m�todo da classe l�gica
			// que foi passada
			// kpto.executa (req,res)
			// o m�todo executa deve retornar um jsp
			String pagina = logica.executa(req, res);

			// obt�m o resquest dispatcher passando
			// para ele a p�gina retornada do m�todo
			// executa e encaminha us�rios
			req.getRequestDispatcher(pagina).forward(req, res);

		} catch (Exception e) {
			throw new ServletException("A l�gica de neg�cios causou uma exece��o", e);
		}

	}
}